// Alternatively, this stuff could live in the store and be dynamically modified

// Determines which buckets are listed
export const buckets = [{
  title: 'Todo',
  name: 'todo'
}, {
  name: 'in-progress',
  title: 'In Progress'
}, {
  name: 'complete',
  title: 'Done'
}];

// buckets.push({
//   name: 'archive',
//   title: 'Trash can'
// })

// Determines the order in which cards are moved from left to right
export const flow = [
  'todo',
  'in-progress',
  'complete',
  'archive',
  'todo'
]

export const stateToLabel = {
  'todo': ['Start', 'play_circle_filled'],
  'in-progress': ['Finish', 'check_circle'],
  'complete': ['Archive', 'archive'],
  'archive': ['Start Over', 'refresh'],
}

// Alternatively everything above could be combined to something like
// {
//   todo: {
//       name: '',
//       title: '',
//       label: '',
//       icon: '',
//       nextBucket: ''
//   }
//   ...
// }
