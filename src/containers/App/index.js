import React, { Component } from 'react';
import StartPage from 'containers/StartPage'
import { Provider } from 'react-redux'
import store from 'store'
import Header from 'components/Header'
import { ThemeProvider } from 'styled-components'
import { Wrapper } from './styles'
import { createMuiTheme } from '@material-ui/core/styles';
const theme = createMuiTheme();

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ThemeProvider theme={theme}>
          <Wrapper>
            <Header />
            <StartPage />
          </Wrapper>
        </ThemeProvider>
      </Provider>
    );
  }
}

export default App;
