import React from 'react';
import TodoList from 'modules/TodoList';
import AddButton from 'components/AddButton';
import AddModal from 'components/AddModal';
import enhance from './enhance';
import {
  Wrapper,
  Main,
  Side
} from './style';

const Board = ({
  tasks,
  actions
}) => (
  <Wrapper>
    <Side>
      <AddButton
        Modal={AddModal}
        onSubmit={actions.addTask}
      />
    </Side>
    <Main>
      <TodoList
        tasks={tasks}
        moveTask={actions.moveTask}
      />
    </Main>
  </Wrapper>
);

export default enhance(Board);
