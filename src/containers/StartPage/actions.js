import * as Types from './constants';
const TASKS_URL = 'https://private-caaa7-versustracker.apiary-mock.com/tasks';

export const loadTasks = () => {
  return dispatch => {
    dispatch({ type: Types.LOAD_TASKS_REQUEST });

    // Create a minimum delay to prevent flicker
    const delay = new Promise(resolve => setTimeout(resolve, 1000));

    fetch(TASKS_URL)
      .then(response => {
        return response.json()
      })
      .then(json => {
        return delay.then(() => {
          dispatch({
            type: Types.LOAD_TASKS_SUCCESS,
            payload: json
          })
        })
      });
  }
}

export const addTask = (name, description) => {
  return {
    type: Types.ADD_TASK,
    name,
    description
  }
}

export const moveTask = (id, currentBucket, targetBucket, targetIndex) => {
  return {
    type: Types.MOVE_TASK,
    id,
    currentBucket,
    targetBucket,
    targetIndex
  }
}
