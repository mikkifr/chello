import * as Types from './constants'
import { fromJS } from 'immutable'
import _ from 'lodash'

const initialState = fromJS({
  buckets: {}
})

const todoReducer = (state = initialState, action) => {
  switch (action.type) {

  case Types.LOAD_TASKS_REQUEST:
    return state.set('loading', true);

  case Types.LOAD_TASKS_SUCCESS:
    return state
      .set('loading', false)
      .set('buckets', fromJS(
        _.groupBy(action.payload.tasks, 'state')
      ));

  case Types.ADD_TASK:
    return state
      .updateIn(['buckets', 'todo'], list => {
        return list.push(fromJS({
          id: `new_task_${_.uniqueId()}`,
          taskName: action.name,
          taskDescription: action.description,
          state: 'todo'
        }));
      });

  case Types.MOVE_TASK:
    // this could probably use some optimization

    let currentBucket = state.getIn(['buckets', action.currentBucket]);
    let targetBucket = state.getIn(['buckets', action.targetBucket]);

    // get the index of the current task and pull it out of the current bucket
    const taskIndex = currentBucket.findIndex(task => task.get('id') === action.id);
    let task = currentBucket.get(taskIndex);

    // remove task from current bucket
    currentBucket = currentBucket.splice(taskIndex, 1);

    // update the state of the current task to be that of the target bucket
    task = task.set('state', action.targetBucket);

    if(action.currentBucket !== action.targetBucket) {
      const targetIndex = action.targetIndex || 0
      targetBucket = targetBucket.insert(targetIndex, task);

      return state
        .setIn(['buckets', action.targetBucket], targetBucket)
        .setIn(['buckets', action.currentBucket], currentBucket);
    } else {
      // this commentary could be improved...

      // if the index of the task being moved is less than the target index,
      // that means the task has been moved up, in which case an insertion is
      // fine; insertion will automatically shift down all items after the point
      // of insertion
      if(taskIndex >= action.targetIndex) {
        currentBucket = currentBucket.insert(action.targetIndex, task);
      }
      // else if the index of the task being moved is less than the target index
      // that means a task has been moved down, in which case we need to take
      // into consideration that since the item at this point has already been
      // removed from the array, reducing the number of items in the array
      // before the target position;
      // to accomodate for this, reduce target index by 1
       else {
        currentBucket = currentBucket.insert(action.targetIndex - 1, task);
      }

      return state.setIn(['buckets', action.currentBucket], currentBucket);
    }

    default:
      return state
  }
}

export default todoReducer
