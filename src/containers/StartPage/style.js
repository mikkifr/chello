import styled from 'styled-components'
import Grid from '@material-ui/core/Grid';

export const Wrapper = styled(Grid).attrs({
  container: true
})`
  && {
    flex-grow: 1;
    max-width: 1200px;
    margin: 0 auto;
  }
`

export const Side = styled(Grid).attrs({
  item: true
})`
  && {
    padding-top: ${p => p.theme.spacing.unit * 12}px;
    padding-right: ${p => p.theme.spacing.unit * 2}px;
  }
`

export const Main = styled(Grid).attrs({
  xs: true,
  item: true
})`
  && {
    display: flex;
  }
`
