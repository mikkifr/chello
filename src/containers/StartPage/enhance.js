import { bindActionCreators } from "redux";
import { connect } from 'react-redux'
import { compose, lifecycle, branch, renderComponent } from 'recompose'
import * as actions from './actions'
import { toJS } from 'components/toJS'
import Loader from 'components/Loader'

const mapStateToProps = (state) => {
  return {
    tasks: state.tasks
  }
}

const mapPropsToDispatch = dispatch => ({
  actions: bindActionCreators(actions, dispatch)
});

const enhance = compose(
  connect(mapStateToProps, mapPropsToDispatch),
  toJS,
  lifecycle({
    componentDidMount: function() {
      this.props.actions.loadTasks()
    }
  }),
  branch(
    ({ tasks }) => tasks.loading,
    renderComponent(Loader)
  ),
);

export default enhance;
