export const LOAD_TASKS_SUCCESS = 'app/LOAD_TASKS_SUCCESS';
export const LOAD_TASKS_REQUEST = 'app/LOAD_TASKS_REQUEST';
export const MOVE_TASK = 'app/MOVE_TASK';
export const ADD_TASK = 'app/ADD_TASK';
