import { combineReducers } from 'redux'
import boardReducer from './containers/StartPage/reducer'

const rootReducer = combineReducers({
  tasks: boardReducer
})

export default rootReducer
