import React, { Fragment } from 'react';
import enhance from './enhance'
import { Columns, Column } from './styles';
import Bucket from 'components/Bucket';
import Task from 'components/Task';
import DropZone from 'components/DropArea';
import { buckets } from 'config';

const Todo = ({
  tasks,
  moveTask
}) => (
  <Columns>
    {buckets.map(({title, name}) => (
      <Column key={name}>
        <Bucket title={title} name={name}>
          <DropZone
            index={0}
            lastChild={(tasks.buckets[name] || []).length === 0}
            targetBucket={name}
            moveTask={moveTask}
          />
          {(tasks.buckets[name] || []).map((item, idx) => (
            <Fragment key={item.id}>
              <Task
                {...item}
                index={idx}
                moveTask={moveTask}
              />
              <DropZone
                index={idx + 1}
                lastChild={idx === tasks.buckets[name].length - 1}
                targetBucket={name}
                moveTask={moveTask}
              />
            </Fragment>
          ))}
        </Bucket>
      </Column>
    ))}
  </Columns>
);

export default enhance(Todo);
