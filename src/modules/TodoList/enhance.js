import { compose } from 'recompose'
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

const enhance = compose(
  DragDropContext(HTML5Backend)
);

export default enhance;
