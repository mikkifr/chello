import styled from 'styled-components'
import Grid from '@material-ui/core/Grid';

export const Columns = styled(Grid).attrs({
  container: true
})`
  max-width: 1400px;
  margin: 0 auto;
`

export const Column = styled(Grid).attrs({
  item: true,
  xs: true
})`
  flex-grow: 1;
  align-items: strech;
  display: flex;
`
