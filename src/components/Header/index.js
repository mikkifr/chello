import React from 'react';
import {
  Wrapper,
  Content,
  Title
} from './styles';

const Header = () => (
  <Wrapper>
    <Content>
      <Title>
        <img
          src={require('./chello.png')}
          alt="Chello"
        />
        Chello
      </Title>
    </Content>
  </Wrapper>
);

export default Header;
