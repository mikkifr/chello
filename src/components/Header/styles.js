import styled from 'styled-components';

export const Wrapper = styled.div`
  background: #0003;
  box-shadow: 0 2px 2px #0006;
`

export const Content = styled.div`
  margin: auto;
  padding: 10px 32px;
`

export const Title = styled.h3`
  font-size: 20px;
  color: #FFFD;
  letter-spacing: 2px;
  font-family: roboto;
  text-transform: uppercase;
  line-height: 32px;

  img {
    vertical-align: middle;
    margin: -5px 5px 0 0;
  }
`
