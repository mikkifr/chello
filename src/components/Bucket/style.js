import styled from 'styled-components'
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

export const Wrapper = styled(Grid).attrs({
  direction: 'column',
  container: true
})`
  && {
    display: flex;
    flex-wrap: nowrap;
    padding: ${p => p.theme.spacing.unit * 2}px;
    padding-top: ${p => p.theme.spacing.unit * 6}px;
  }
`

export const Header = styled(Grid).attrs({
  item: true
})`
  && {
    flexGrow: 0;
    margin: 0;
    padding-left: ${p => p.theme.spacing.unit * 2};
    padding-right: ${p => p.theme.spacing.unit * 2};
  }
`

export const Title = styled(Typography).attrs({
  variant: 'title'
})`
  && {
    text-transform: uppercase;
    font-size: 24px;
    line-height: 40px;
    font-family: roboto;
    font-weight: 300;
    color: white;
  }
`

export const Content = styled(Grid).attrs({
  item: true
})`
  && {
    flexGrow: 1;
  }
`
