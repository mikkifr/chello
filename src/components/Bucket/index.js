import React from 'react';
import {
  Wrapper,
  Header,
  Title,
  Content
} from './style';

const Bucket = ({
  name,
  title,
  children
}) => (
  <Wrapper>
    <Header>
      <Title>
        {title}
      </Title>
    </Header>
    <Content>
      {children}
    </Content>
  </Wrapper>
);

export default Bucket;
