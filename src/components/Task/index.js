import React from 'react';
import enhance from './enhance';
import { stateToLabel } from '../../config';
import {
  Title,
  Number,
  Description,
  Actions,
  Content,
  Wrapper,
  AdvanceButton,
  AdvanceIcon
} from './styles';

const Task = ({
  state,
  index,
  taskName,
  isDragging,
  advanceItem,
  taskDescription,
  connectDragSource,
  ...rest
}) => {
  return connectDragSource(
    <div>
      <Wrapper isDragging={isDragging}>
        <Content>
          <Number>
            {index + 1}
          </Number>
          <Title>
            {taskName}
          </Title>
          <Description>
            {taskDescription}
          </Description>
        </Content>
        <Actions>
          {stateToLabel[state] && (
            <AdvanceButton onClick={advanceItem}>
              {stateToLabel[state][0]}
              <AdvanceIcon>
                {stateToLabel[state][1]}
              </AdvanceIcon>
            </AdvanceButton>
          )}
        </Actions>
      </Wrapper>
    </div>
  )
}

export default enhance(Task);
