import styled from 'styled-components'
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';

export const Wrapper = styled(Card)`
  && {
    position: relative;
    background: #FFFD;
    transition: all 200ms;
    cursor: pointer;

    ${props => props.isDragging && `
      opacity: .5;
    `}
  }

  &&:hover {
    background: #fff;
  }

  &&:before {
    content: '';
    background: #C80000;
    display: block;
    float:left;
    position: absolute;
    top: 0;
    left: 0;
    height: 100%;
  }
`

export const Actions = styled(CardActions)`
  && {
    justify-content: flex-end;
    padding: 0 8px 8px;
  }
`

export const Content = styled(CardContent)`
  && {
    padding-bottom: 0;
  }
`

export const Title = styled(Typography).attrs({
  color: 'textPrimary'
})`
  && {
    font-size: 16px
  }
`

export const Number = styled(Typography)`
  && {
    position: absolute;
    top: ${p => p.theme.spacing.unit * 2}px;
    right: ${p => p.theme.spacing.unit * 3}px;
    line-height: 20px;
    text-align: center;
    font-weight: bold;
    color: #bbb
  }
`

export const Description = styled(Typography).attrs({
  component: 'p',
  color: 'textSecondary'
})``

export const AdvanceButton = styled(Button).attrs({
  size: 'small',
  mini: true
})`
  && {
    color: #999;
    margin: 0;
    min-width: auto;
  }

  &&:hover {
    color: #303;
  }
`

export const AdvanceIcon = styled(Icon)`
  && {
    margin-left: 5px;
  }
`
