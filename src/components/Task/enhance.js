import ReactDOM from 'react-dom';
import { toClass, compose, withHandlers } from 'recompose';
import { DragSource } from 'react-dnd';
import { flow } from '../../config';

const cardSource = {
  beginDrag(props, monitor, component) {
    const el = ReactDOM.findDOMNode(component);

    return {
      id: props.id,
      height: el.clientHeight,
      currentBucket: props.state
    }
  }
};

function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  }
}

export default compose(
  DragSource('TASK', cardSource, collect),
  withHandlers({
      advanceItem: ({ moveTask, state, id }) => event => {
        const targetIndex = flow.indexOf(state);
        const targetBucket = flow[targetIndex + 1];

        moveTask(id, state, targetBucket);
      }
  }),
  toClass
);
