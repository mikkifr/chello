import styled from 'styled-components'
import MaterialButton from '@material-ui/core/Button';

export const Button = styled(MaterialButton).attrs({
  variant: 'extendedFab',
  color: 'primary'
})`
   && {
     background: #5500dd;
     border-radius: 6px;
   }

   &&:hover {
     background: #dd0099;
   }
`
