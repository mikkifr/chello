import { compose, withState, withHandlers } from 'recompose';

const enhance = compose(
  withState('open', 'setVisibility', false),
  withHandlers({
    handleOpen: ({ setVisibility }) => () => setVisibility(true),
    handleClose: ({ setVisibility }) => () => setVisibility(false)
  })
);

export default enhance;
