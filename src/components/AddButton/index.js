import React from 'react';
import Icon from '@material-ui/core/Icon';
import enhance from './enhance'
import {
  Button
} from './styles'

const AddButton = ({
  open,
  Modal,
  onSubmit,
  handleOpen,
  handleClose
}) => (
  <div>
    <Button onClick={handleOpen}>
      <Icon>add</Icon>
    </Button>
    <Modal
      open={open}
      onSubmit={onSubmit}
      handleClose={handleClose}
    />
  </div>
);

export default enhance(AddButton);
