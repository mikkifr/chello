import styled from 'styled-components'
import Typography from '@material-ui/core/Typography';
import MaterialButton from '@material-ui/core/Button';
import MaterialFormControl from '@material-ui/core/FormControl';
import IconButton from '@material-ui/core/IconButton';

export const Content = styled.div`
  top: 50%;
  left: 50%;
  position: absolute;
  background-color: ${p => p.theme.palette.background.paper};
  box-shadow: ${p => p.theme.shadows[5]};
  padding: ${p => p.theme.spacing.unit * 4}px;
  outline: none;
  transform: translate(-50%, -50%);
  width: ${p => p.theme.spacing.unit * 50}px;
`

export const Title = styled(Typography).attrs({
  variant: 'title'
})`
  && {
    margin-bottom: 20px;
  }
`

export const Actions = styled.div`
  display: flex;
  justify-content: flex-end;
  margin: 20px 0 0;
`

export const Button = styled(MaterialButton).attrs({
  variant: 'contained',
  color: 'primary'
})`
  && {
    box-shadow: none;
  }
`

export const FormControl = styled(MaterialFormControl).attrs({
  fullWidth: true
})`
  &&:not(:last-child) {
    margin-bottom: 20px;
  }
`

export const CloseButton = styled(IconButton).attrs({
  size: 'small'
})`
  && {
    position: absolute;
    top: 8px
    right: 8px;
  }
`
