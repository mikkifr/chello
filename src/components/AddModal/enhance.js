import { compose, withStateHandlers, withHandlers } from 'recompose'

const enhance = compose(
  withStateHandlers({
    name: '',
    description: '',
    errorFields: []
  }, {
    // Keep track of field values
    // TODO: maybe change to handleInput Change
    handleChange: () => ({ target }) => ({
      [target.name]: target.value
    }),
    triggerError: () => (errorFields) => ({
      errorFields
    })
  }),
  withHandlers({
    handleSubmit: ({
      name,
      description,
      onSubmit,
      handleClose,
      triggerError
    }) => (e) => {
      // Reeeeaally basic validation
      const errors = [];

      if(!name) errors.push('name');
      if(!description) errors.push('description');

      if(errors.length) {
        triggerError(errors);
        return;
      }

      onSubmit(name, description);
      handleClose();
    }
  })
)

export default enhance
