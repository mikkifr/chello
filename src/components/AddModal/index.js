import React from 'react'
import enhance from './enhance'
import Modal from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField';
import Icon from '@material-ui/icons/Close'
import {
  Content,
  Title,
  Actions,
  Button,
  FormControl,
  CloseButton
} from './styles';

const AddModal = ({
  open,
  name,
  description,
  errorFields,
  handleClose,
  handleSubmit,
  handleChange,
  ...rest
}) => (
  <Modal open={open} onClose={handleClose}>
    <Content>
      <Title>Add New Task</Title>
      <CloseButton onClick={handleClose}>
        <Icon />
      </CloseButton>
      <FormControl>
        <TextField
          name="name"
          label="Name"
          value={name}
          onChange={handleChange}
          error={errorFields.includes('name')}
        />
      </FormControl>
      <FormControl>
        <TextField
          name="description"
          label="Description"
          value={description}
          onChange={handleChange}
          error={errorFields.includes('description')}
        />
      </FormControl>
      <Actions>
        <Button onClick={handleSubmit}>
          Add Task
        </Button>
      </Actions>
    </Content>
  </Modal>
)

export default enhance(AddModal);
