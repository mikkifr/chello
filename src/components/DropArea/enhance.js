import { DropTarget } from 'react-dnd';
import { compose } from 'recompose';

const dropTarget = {
  drop: function(props, monitor) {
    const { id, currentBucket } = monitor.getItem();
    const { index, targetBucket, moveTask } = props;

    moveTask(id, currentBucket, targetBucket, index);
  }
}

const collect = (connect, monitor) => {
  const item = monitor.getItem();

  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    height: item ? item.height : null
  }
}

const enhance = compose(
  DropTarget('TASK', dropTarget, collect)
)

export default enhance
