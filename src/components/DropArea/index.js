import React from 'react';
import enhance from './enhance';
import { Zone } from './styles';

const DropArea = ({
  isOver,
  height,
  lastChild,
  connectDropTarget
}) => {
  return connectDropTarget(
    <div>
      <Zone
        lastChild={lastChild}
        height={height}
        isOver={isOver}
      />
    </div>
  )
}

export default enhance(DropArea);
