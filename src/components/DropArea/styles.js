import styled from 'styled-components';
import Card from '@material-ui/core/Card';

export const Zone = styled(Card)`
  && {
    padding: 8px 0;
    margin: -8px 0;
    background: #0000;
    z-index: 10;
    position: relative;
    box-shadow: none;
  }

  &&:after {
    content: '';
    display: block;
    height: 10px;
    background: #FFF9;
    opacity: 0;
    border-radius: ${p => p.theme.shape.borderRadius}px;

    ${props => props.isOver && `
      margin: 10px 0;
      transition: all 200ms;
      height: ${props.height}px;
      opacity: 1;
    `}

    ${props => props.lastChild && `
      height: ${props.height ? props.height : 100 }px;
    `
  }
`
