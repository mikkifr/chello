import styled from 'styled-components';

export const Wrapper = styled.div`
  box-shadow: 0 0 5px rgba(0,0,0,.8);
  display: flex;
  margin: auto;
  width: 130px;
  height: 130px;
  background: #FFF9;
  border-radius: 100%;
  align-content: justify-content;
`

export const Image = styled.img`
  max-width: 100%;
  max-height: 100%;
  margin: auto;

`
