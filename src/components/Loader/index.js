import React from 'react';
import {
  Wrapper,
  Image
} from './styles'

const Loader = () => (
  <Wrapper>
    <Image src={require('./violin.gif')} />
  </Wrapper>
)

export default Loader
